import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OpCardModule } from 'op-card';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppModuleRouting } from './app.routing';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule,
    AppModuleRouting,
    OpCardModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
