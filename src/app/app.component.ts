import { Component } from '@angular/core';
import { NgopCardEcom } from 'op-card';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'op-card-app';

  data2: NgopCardEcom = {
    price:'12 €',
    rating: 4.5,
    name: 'Restaurant Agadir',
    type: 'Cafe, Bakery ',
    imgUrl: 'http://via.placeholder.com/430x300',
    info:'8000 Agadir,Morocco, best resturant in Agadir city',
    ingred: [
      { titre: 'Type', value: 'Brunch, Lunch, Dinner' },
      { titre: 'Taxi', value: 'No' },
      { titre: 'Credit-cards', value: 'Yes' },
      { titre: 'Wifi', value: 'Yes' },
    ]
  };
  

}
