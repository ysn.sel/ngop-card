import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";

const routes: Routes = [
    {
        path: '',
        component: AppComponent,
    },
    
];

export const AppModuleRouting: ModuleWithProviders = RouterModule.forRoot(routes);