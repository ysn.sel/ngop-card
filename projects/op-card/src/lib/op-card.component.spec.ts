import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpCardComponent } from './op-card.component';

describe('OpCardComponent', () => {
  let component: OpCardComponent;
  let fixture: ComponentFixture<OpCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
