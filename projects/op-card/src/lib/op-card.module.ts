import { NgModule } from '@angular/core';
import { OpCardComponent } from './op-card.component';
import { OpCardEmptyComponent } from './components/op-card-empty/op-card-empty.component';
import { OpCardEcomComponent } from './components/op-card-ecom/op-card-ecom.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [OpCardComponent, OpCardEmptyComponent, OpCardEcomComponent],
  exports: [OpCardComponent, OpCardEmptyComponent,OpCardEcomComponent]
})
export class OpCardModule { }
