import { Component, OnInit, Input, Renderer2, ElementRef } from '@angular/core';

@Component({
  selector: 'ngop-cardEcom',
  templateUrl: './op-card-ecom.component.html',
  styleUrls: ['./op-card-ecom.component.css']
})
export class OpCardEcomComponent implements OnInit {

  @Input('color') color?:String;
  @Input('btnTitle') btnTitle?:String;
  @Input('goto') goto?:String;

  @Input('data') data:NgopCardEcom;
  
  constructor( private renderer: Renderer2, private _elRef: ElementRef) { }

  ngOnInit() {
    let backStyle="url(http://via.placeholder.com/430x300) no-repeat center/cover";
    if(this.data.imgUrl!=null)
      backStyle="url("+this.data.imgUrl+") no-repeat center/cover";
    const wrapper=this._elRef.nativeElement.querySelector('.ngno-widget .photo');
    this.renderer.setStyle(wrapper, 'background', backStyle);
  }

}
export class NgopCardEcom {
  price?: String;
  rating?: number;
  type?: String;
  name?: String;
  imgUrl?: String;
  info?:String;
  ingred?:{titre:String, value:String}[];
}