import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpCardEcomComponent } from './op-card-ecom.component';

describe('OpCardEcomComponent', () => {
  let component: OpCardEcomComponent;
  let fixture: ComponentFixture<OpCardEcomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpCardEcomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpCardEcomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
