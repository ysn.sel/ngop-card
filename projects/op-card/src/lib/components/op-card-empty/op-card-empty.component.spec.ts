import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpCardEmptyComponent } from './op-card-empty.component';

describe('OpCardEmptyComponent', () => {
  let component: OpCardEmptyComponent;
  let fixture: ComponentFixture<OpCardEmptyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpCardEmptyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpCardEmptyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
