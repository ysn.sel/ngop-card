import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngop-cardEmpty',
  templateUrl: './op-card-empty.component.html',
  styleUrls: ['./op-card-empty.component.css']
})
export class OpCardEmptyComponent implements OnInit {

  @Input("shadow") shadow:number;
  @Input("hovered") hovered:boolean;
  constructor() { }

  ngOnInit() {
  }

}
