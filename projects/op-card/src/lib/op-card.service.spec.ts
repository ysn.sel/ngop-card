import { TestBed, inject } from '@angular/core/testing';

import { OpCardService } from './op-card.service';

describe('OpCardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpCardService]
    });
  });

  it('should be created', inject([OpCardService], (service: OpCardService) => {
    expect(service).toBeTruthy();
  }));
});
